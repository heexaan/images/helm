FROM alpine:3.18

ARG HELM_VERSION=helm-v3.13.0
ARG K8S_VERSION=v1.28.2

RUN apk add --update ca-certificates \
 && apk add --update -t deps curl git \
 && apk add --update gettext tar gzip

RUN curl -L https://dl.k8s.io/release/${K8S_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl
RUN curl -L https://get.helm.sh/${HELM_VERSION}-linux-amd64.tar.gz | tar xz && mv linux-amd64/helm /bin/helm && rm -rf linux-amd64 \
 && chmod +x /usr/local/bin/kubectl

CMD [ "helm" ]
